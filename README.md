# Knapsack Problem
By Amin Malekpour
---
# Technology, Tool and framework
These technologies, tools and frameworks have been used in the project:
1. Java 8
2. TestNG
3. Maven

---

# Project structure
I have used Maven as the build tool.
Source codes are located under the “src/main/java” folder.
---

# Test
I have written some useful test classes under the “src/test/java” folder.

---

# How to run the application
Use the following commands to build and run the application:
1. mvn clean install

---

