package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Knapsack;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.*;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class DeserializerTest {

    private Deserializer deserializer = new DeserializerImpl();

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenFileIsNotValid_thenReturnAPIException() {
        deserializer.deserialize("invalid path");
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenEmptyFile_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/empty.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenThereIsEmptyLineBetweenLines_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputWithEmptyLine.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenThereIsLineWithoutColonBetweenLines_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputWithoutColon.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenInvalidCapacity_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidCapacity.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenCapacityLessThanZero_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputCapacityLessThanZero.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenWithoutItem_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputWithoutItem.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenInvalidItem1_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidItem_1.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenInvalidItem2_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidItem_2.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenInvalidItemIndex_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidIndex.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenInvalidItemWeight_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidWeight.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenInvalidItemPrice_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidPrice.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenItemWeightLessThanZero_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputWeightLessThanZero.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void convertToKnapsacks_whenItemPriceLessThanZero_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputPriceLessThanZero.txt").toURI()).toString();
        deserializer.deserialize(path);
    }

    @Test
    public void convertToKnapsacks_whenValidInputWithItemZeroPrice_thenDoNotAddItemWithZeroPrice() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/validInputWithZeroPrice.txt").toURI()).toString();
        List<Knapsack> expected = createExpectedResultWithSize1();
        List<Knapsack> actual = deserializer.deserialize(path);

        Assert.assertEquals(actual.size(), expected.size());
        Assert.assertEquals(actual.get(0).getCapacity(), expected.get(0).getCapacity());
        Assert.assertEquals(actual.get(0).getItems().size(), expected.get(0).getItems().size());

        Item expectedItem_1 = expected.get(0).getItems().iterator().next();
        Item actualItem_1 = actual.get(0).getItems().iterator().next();
        Assert.assertEquals(actualItem_1.getIndex(), expectedItem_1.getIndex());
        Assert.assertEquals(actualItem_1.getWeight(), expectedItem_1.getWeight());
        Assert.assertEquals(actualItem_1.getPrice(), expectedItem_1.getPrice());
    }

    private List<Knapsack> createExpectedResultWithSize1() {
        Set<Item> items = new LinkedHashSet<>();
        items.add(new Item(2, new BigDecimal("88.62"), new BigDecimal("98")));
        return Arrays.asList( new Knapsack(items, new BigDecimal("81")));
    }

    @Test
    public void convertToKnapsacks_whenValidInputWith2Lines_thenReturnListWithSize2() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/validInputWith2Lines.txt").toURI()).toString();
        List<Knapsack> expected = createExpectedResultWithSize2();
        List<Knapsack> actual = deserializer.deserialize(path);

        //check element 1
        Assert.assertEquals(actual.size(), expected.size());
        Assert.assertEquals(actual.get(0).getCapacity(), expected.get(0).getCapacity());
        Assert.assertEquals(actual.get(0).getItems().size(), expected.get(0).getItems().size());
        Item expectedItem_1 = expected.get(0).getItems().iterator().next();
        Item actualItem_1 = actual.get(0).getItems().iterator().next();
        Assert.assertEquals(actualItem_1.getIndex(), expectedItem_1.getIndex());
        Assert.assertEquals(actualItem_1.getWeight(), expectedItem_1.getWeight());
        Assert.assertEquals(actualItem_1.getPrice(), expectedItem_1.getPrice());

        //check element 2
        Assert.assertEquals(actual.get(1).getCapacity(), expected.get(1).getCapacity());
        Assert.assertEquals(actual.get(1).getItems().size(), expected.get(1).getItems().size());
        Item expectedItem_2 = expected.get(1).getItems().iterator().next();
        Item actualItem_2 = actual.get(1).getItems().iterator().next();
        Assert.assertEquals(actualItem_2.getIndex(), expectedItem_2.getIndex());
        Assert.assertEquals(actualItem_2.getWeight(), expectedItem_2.getWeight());
        Assert.assertEquals(actualItem_2.getPrice(), expectedItem_2.getPrice());
        Item expectedItem_3 = expected.get(1).getItems().iterator().next();
        Item actualItem_3 = actual.get(1).getItems().iterator().next();
        Assert.assertEquals(actualItem_3.getIndex(), expectedItem_3.getIndex());
        Assert.assertEquals(actualItem_3.getWeight(), expectedItem_3.getWeight());
        Assert.assertEquals(actualItem_3.getPrice(), expectedItem_3.getPrice());
    }

    private List<Knapsack> createExpectedResultWithSize2() {
        Set<Item> items_1 = new LinkedHashSet<>();
        items_1.add(new Item(1, new BigDecimal("45.52"), new BigDecimal("69")));
        Set<Item> items_2 = new LinkedHashSet<>();
        items_2.add(new Item(1, new BigDecimal("85.31"), new BigDecimal("29")));
        items_2.add(new Item(2, new BigDecimal("14.55"), new BigDecimal("74")));
        return Arrays.asList( new Knapsack(items_1, new BigDecimal("55")),
                new Knapsack(items_2, new BigDecimal("75")));
    }
}
