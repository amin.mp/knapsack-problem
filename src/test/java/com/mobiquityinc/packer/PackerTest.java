package com.mobiquityinc.packer;

import com.mobiquityinc.config.Constants;
import com.mobiquityinc.exception.APIException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class PackerTest {

    @Test(expectedExceptions = APIException.class)
    public void pack_whenFileIsNotValid_thenReturnAPIException() {
        Packer.pack("invalid path");
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenEmptyFile_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/empty.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenThereIsEmptyLineBetweenLines_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputWithEmptyLine.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenThereIsLineWithoutColonBetweenLines_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputWithoutColon.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenInvalidCapacity_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidCapacity.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenCapacityLessThanZero_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputCapacityLessThanZero.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenWithoutItem_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputWithoutItem.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenInvalidItem1_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidItem_1.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenInvalidItem2_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidItem_2.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenInvalidItemIndex_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidIndex.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenInvalidItemWeight_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidWeight.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenInvalidItemPrice_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputInvalidPrice.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenItemWeightLessThanZero_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputWeightLessThanZero.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test(expectedExceptions = APIException.class)
    public void pack_whenItemPriceLessThanZero_thenReturnAPIException() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/inputPriceLessThanZero.txt").toURI()).toString();
        Packer.pack(path);
    }

    @Test
    public void pack_whenHas4Lines_thenReturn4Answers() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/validInputWith4Lines.txt").toURI()).toString();
        String actual = Packer.pack(path);
        String expected = "4\n-\n2,7\n8,9\n";

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void pack_whenHas1Line_thenReturnEmpty() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/validInputWithZeroPrice.txt").toURI()).toString();
        String actual = Packer.pack(path);
        String expected = Constants.NO_ANSWER;

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void pack_whenHas1LineAndItemsWithSameWeight_thenReturnOneItemWithHighestPrice() throws URISyntaxException {
        String path = Paths.get(this.getClass().getResource("/validInputWithSamePrice.txt").toURI()).toString();
        String actual = Packer.pack(path);
        String expected = "5\n";

        Assert.assertEquals(actual, expected);
    }

}
