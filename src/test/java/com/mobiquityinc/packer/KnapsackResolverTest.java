package com.mobiquityinc.packer;

import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Knapsack;
import com.mobiquityinc.model.Option;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class KnapsackResolverTest {

    private KnapsackResolver knapsackResolver = new KnapsackResolverImpl();

    @Test
    public void calculateOne_when6Items_thenReturnItem4() {
        Set<Integer> item = new LinkedHashSet<>();
        item.add(4);
        Option expected = new Option(item,  new BigDecimal("8.70"), new BigDecimal("76"));
        Option actual = knapsackResolver.resolve(createKnapsack_1());

        Assert.assertEquals(actual.getItems().size(), expected.getItems().size());
        Assert.assertEquals(actual.getItems().iterator().next(), expected.getItems().iterator().next());
        Assert.assertEquals(actual.getCapacity(), expected.getCapacity());
        Assert.assertEquals(actual.getPrice(), expected.getPrice());
    }

    @Test
    public void calculateOne_whenOneItemAndWeightIsMoreThanCapacity_thenReturnEmpty() {
        Option actual = knapsackResolver.resolve(createKnapsack_2());
        Option expected = new Option(new HashSet<>(), new BigDecimal("8"), new BigDecimal("0"));

        Assert.assertEquals(actual.getItems().size(), expected.getItems().size());
        Assert.assertEquals(actual.getCapacity(), expected.getCapacity());
        Assert.assertEquals(actual.getPrice(), expected.getPrice());
    }

    @Test
    public void calculateOne_when9Items_thenReturnItem2And7() {
        Set<Integer> item = new LinkedHashSet<>();
        item.add(2);
        item.add(7);
        Option expected = new Option(item,  new BigDecimal("0.43"), new BigDecimal("148"));
        Option actual = knapsackResolver.resolve(createKnapsack_3());

        Assert.assertEquals(actual.getItems().size(), expected.getItems().size());
        Assert.assertEquals(actual.getItems().iterator().next(), expected.getItems().iterator().next());
        Assert.assertEquals(actual.getItems().iterator().next(), expected.getItems().iterator().next());
        Assert.assertEquals(actual.getCapacity(), expected.getCapacity());
        Assert.assertEquals(actual.getPrice(), expected.getPrice());
    }

    @Test
    public void calculateOne_when9Items_thenReturnItem8And9() {
        Set<Integer> item = new LinkedHashSet<>();
        item.add(8);
        item.add(9);
        Option expected = new Option(item,  new BigDecimal("29.88"), new BigDecimal("143"));
        Option actual = knapsackResolver.resolve(createKnapsack_4());

        Assert.assertEquals(actual.getItems().size(), expected.getItems().size());
        Assert.assertEquals(actual.getItems().iterator().next(), expected.getItems().iterator().next());
        Assert.assertEquals(actual.getItems().iterator().next(), expected.getItems().iterator().next());
        Assert.assertEquals(actual.getCapacity(), expected.getCapacity());
        Assert.assertEquals(actual.getPrice(), expected.getPrice());
    }

    protected Knapsack createKnapsack_1() {
        Set<Item> items = new LinkedHashSet<>();
        items.add(new Item(1, new BigDecimal("53.38"), new BigDecimal("45")));
        items.add(new Item(2, new BigDecimal("88.62"), new BigDecimal("98")));
        items.add(new Item(3, new BigDecimal("78.48"), new BigDecimal("3")));
        items.add(new Item(4, new BigDecimal("72.30"), new BigDecimal("76")));
        items.add(new Item(5, new BigDecimal("30.18"), new BigDecimal("9")));
        items.add(new Item(6, new BigDecimal("46.34"), new BigDecimal("48")));
        return new Knapsack(items, new BigDecimal("81"));
    }

    protected Knapsack createKnapsack_2() {
        Set<Item> items = new LinkedHashSet<>();
        items.add(new Item(1, new BigDecimal("15.3"), new BigDecimal("34")));
        return new Knapsack(items, new BigDecimal("8"));
    }

    protected Knapsack createKnapsack_3() {
        Set<Item> items = new LinkedHashSet<>();
        items.add(new Item(1, new BigDecimal("85.31"), new BigDecimal("29")));
        items.add(new Item(2, new BigDecimal("14.55"), new BigDecimal("74")));
        items.add(new Item(3, new BigDecimal("3.98"), new BigDecimal("16")));
        items.add(new Item(4, new BigDecimal("26.24"), new BigDecimal("55")));
        items.add(new Item(5, new BigDecimal("63.69"), new BigDecimal("52")));
        items.add(new Item(6, new BigDecimal("76.25"), new BigDecimal("75")));
        items.add(new Item(7, new BigDecimal("60.02"), new BigDecimal("74")));
        items.add(new Item(8, new BigDecimal("93.18"), new BigDecimal("35")));
        items.add(new Item(9, new BigDecimal("89.95"), new BigDecimal("78")));
        return new Knapsack(items, new BigDecimal("75"));
    }

    protected Knapsack createKnapsack_4() {
        Set<Item> items = new LinkedHashSet<>();
        items.add(new Item(1, new BigDecimal("90.72"), new BigDecimal("13")));
        items.add(new Item(2, new BigDecimal("33.80"), new BigDecimal("40")));
        items.add(new Item(3, new BigDecimal("43.15"), new BigDecimal("10")));
        items.add(new Item(4, new BigDecimal("37.97"), new BigDecimal("16")));
        items.add(new Item(5, new BigDecimal("46.81"), new BigDecimal("36")));
        items.add(new Item(6, new BigDecimal("48.77"), new BigDecimal("79")));
        items.add(new Item(7, new BigDecimal("81.80"), new BigDecimal("45")));
        items.add(new Item(8, new BigDecimal("19.36"), new BigDecimal("79")));
        items.add(new Item(9, new BigDecimal("6.76"), new BigDecimal("64")));
        return new Knapsack(items, new BigDecimal("56"));
    }

}
