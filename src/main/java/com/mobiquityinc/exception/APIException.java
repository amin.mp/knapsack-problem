package com.mobiquityinc.exception;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class APIException extends RuntimeException{

    public APIException(String message) {
        super(message);
    }
}
