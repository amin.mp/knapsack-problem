package com.mobiquityinc.packer;

import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Knapsack;
import com.mobiquityinc.model.Option;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class KnapsackResolverImpl implements KnapsackResolver {

    public Option resolve(Knapsack knapsack){
        Option result = new Option(new LinkedHashSet<>(), knapsack.getCapacity(), new BigDecimal("0"));
        List<Option> options = new ArrayList<>();
        options.add(result);
        List<Item> items = new ArrayList<>(knapsack.getItems());
        items.sort(Item::compareTo);
        for(Item item : items) {
            List<Option> newOptions = new ArrayList<>();
            for(Option currentOption : options) {
                if(item.getWeight().compareTo(currentOption.getCapacity()) > 0) break;
                Option newOption = createNewOption(currentOption, item);
                newOptions.add(newOption);
                result = greaterOptionByPrice(result, newOption);
            }
            options = mergeTwoSortedList(options, newOptions);
        }
        result.setItems(result.getItems().stream().sorted(Integer::compareTo).collect(Collectors.toCollection(LinkedHashSet::new)));
        return result;
    }

    /**
     * Merge two sorted list and return a sorted list.
     * @param list1
     * @param list2
     * @return a sorted list of options
     */
    private List<Option> mergeTwoSortedList(List<Option> list1, List<Option> list2) {
        List<Option> result = new ArrayList<>();
        int index1 = 0;
        int index2 = 0;
        while (index1 < list1.size() || index2 < list2.size()) {
            if (index1 < list1.size() && index2 < list2.size()) {
                Option option1 = list1.get(index1);
                Option option2 = list2.get(index2);
                if (option1.compareTo(option2) <= 0) {
                    result.add(option1);
                    index1++;
                } else {
                    result.add(option2);
                    index2++;
                }
            } else if (index1 < list1.size()) {
                result.add(list1.get(index1));
                index1++;
            } else {
                result.add(list2.get(index2));
                index2++;
            }
        }
        return result;
    }

    /**
     * Compare 2 options and return the one with higher price.
     * If the price are the same it returns the one is lighter.
     * @param result
     * @param option
     * @return Option
     */
    private Option greaterOptionByPrice(Option result, Option option){
        if (option.getPrice().compareTo(result.getPrice()) > 0) return option;
        if (option.getPrice().compareTo(result.getPrice()) == 0 &&
                option.getCapacity().compareTo(result.getCapacity()) > 0)
            return option;

        return result;
    }

    /**
     * Create an option by adding new item to the previous selected items.
     * @param oldOption
     * @param item
     * @return Option
     */
    private Option createNewOption(Option oldOption, Item item) {
        Set<Integer> items = new LinkedHashSet<>();
        items.addAll(oldOption.getItems());
        items.add(item.getIndex());
        BigDecimal capacity = oldOption.getCapacity().subtract(item.getWeight());
        BigDecimal price = oldOption.getPrice().add(item.getPrice());
        return new Option(items, capacity, price);
    }

}
