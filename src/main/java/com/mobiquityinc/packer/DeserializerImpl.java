package com.mobiquityinc.packer;

import com.mobiquityinc.config.Constants;
import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Knapsack;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class DeserializerImpl implements Deserializer {

    public List<Knapsack> deserialize(String filePath) throws APIException {
        File file = new File(filePath);
        try(InputStream stream = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))
        ){
            List<String> lines = readLines(reader);
            return mapToKnapsacks(lines);
        } catch (IOException io){
            io.printStackTrace();
            throw new APIException("File not found.");
        } catch (APIException api){
            api.printStackTrace();
            throw api;
        }
    }

    /**
     * Map lines as String to list of Knapsack object.
     * @param lines
     * @return List of knapsack
     * @throws APIException
     */
    private List<Knapsack> mapToKnapsacks(List<String> lines) throws APIException {
        List<Knapsack> result = new ArrayList<>();
        if(lines.isEmpty()) throw new APIException("File is empty.");
        for(int i=0; i < lines.size(); i++){
            String line = lines.get(i);
            String[] splitLine = line.split(":");
            if(splitLine.length <= 1) throw new APIException(String.format("There is no symbol ':' or no item in line: %s", (i+1)));
            if(splitLine.length > 2) throw new APIException(String.format("There are more than 1 symbol ':' in line: %s", (i+1)));

            BigDecimal capacity;
            try {
                String capacityStr = splitLine[Constants.KNAPSACK_CAPACITY_INDEX].replaceAll(Constants.SPACE, Constants.EMPTY);
                capacity = new BigDecimal(capacityStr);
            } catch (Exception e){
                throw new APIException(String.format("Parameter 'capacity' can not be converted to Double: '%s'", splitLine[Constants.KNAPSACK_CAPACITY_INDEX]));
            }
            if(capacity.compareTo(new BigDecimal("0")) < 0) throw new APIException(String.format("Capacity can not be less than zero. In line: %s", (i+1)));
            Set<Item> itemSet = mapToItems(splitLine[Constants.KNAPSACK_ITEMS_INDEX]);
            result.add(new Knapsack(itemSet, capacity));
        }
        return result;
    }

    /**
     * Map items as String to set of Item object.
     * @param input
     * @return Set of Items
     */
    private Set<Item> mapToItems(String input) {
        Set<Item> itemSet = new LinkedHashSet<>();
        Pattern pattern = Pattern.compile(Constants.PARENTHESIS_REGEX);
        Matcher matcher = pattern.matcher(input);
        boolean found = false;
        while (matcher.find()){
            found = true;
            String itemStr = matcher.group(1);
            Item item = mapToItem(itemStr);
            if(item != null) itemSet.add(item);
        }
        if(!found) throw new APIException("Can not find item: " + input);
        return itemSet;
    }

    /**
     * Map item as String to Item object.
     * @param itemStr
     * @return Item
     * @throws APIException
     */
    private Item mapToItem(String itemStr) throws APIException {
        String[] splits = itemStr.replaceAll(Constants.SPACE, Constants.EMPTY).split(",");
        if(splits.length < 3) throw new APIException(String.format("Can not map to item: '%s' ", itemStr));
        Integer index;
        BigDecimal weight;
        BigDecimal price;
        try {
            index = Integer.valueOf(splits[Constants.ITEM_INDEX]);
            weight = new BigDecimal(splits[Constants.ITEM_WEIGHT_INDEX]);
            price = new BigDecimal(splits[Constants.ITEM_PRICE_INDEX].substring(1));
        } catch (Exception e){
            throw new APIException(String.format("Can not map to number: '%s'", itemStr));
        }
        BigDecimal zero = new BigDecimal("0");
        if(weight.compareTo(zero) < 0) throw new APIException(String.format("Weight can not be less than zero: '%s'", weight.doubleValue()));
        if(price.compareTo(zero) < 0) throw new APIException(String.format("Price can not be less than zero: '%s'", price.doubleValue()));
        if(price.compareTo(zero) == 0) return null;
        return new Item(index, weight, price);
    }

    /**
     * Read from file and return a list of lines as a list of String
     * @param reader
     * @return list of lines
     * @throws IOException
     */
    private List<String> readLines(BufferedReader reader) throws IOException {
        List<String> result = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        return result;
    }

}
