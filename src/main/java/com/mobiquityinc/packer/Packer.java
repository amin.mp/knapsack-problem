package com.mobiquityinc.packer;

import com.mobiquityinc.config.Constants;
import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Knapsack;
import com.mobiquityinc.model.Option;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class Packer {

    /**
     * For quick representing the result in the terminal.
     * @param args
     * @throws URISyntaxException
     * @throws APIException
     */
    public static void main(String[] args) throws URISyntaxException, APIException {
        String path = Paths.get(Packer.class.getResource("/input.txt").toURI()).toString();
        String result = pack(path);
        System.out.println(result);
    }

    /**
     * Read data from a text file and return the best selection of items in order to have maximum price.
     * @param filePath
     * @return final result
     * @throws APIException
     */
    public static String pack(String filePath) throws APIException {
        DeserializerImpl deserializer = new DeserializerImpl();
        List<Knapsack> knapsacks = deserializer.deserialize(filePath);
        return resolveAll(knapsacks);
    }

    /**
     * Calculate all best selections of items in order to have maximum price.
     * @param knapsacks
     * @return All best selections as String
     */
    private static String resolveAll(List<Knapsack> knapsacks){
        KnapsackResolverImpl resolver = new KnapsackResolverImpl();
        StringBuilder result = new StringBuilder();
        for (Knapsack knapsack : knapsacks){
            Option option = resolver.resolve(knapsack);
            String tempResult = optionToString(option);
            result.append(tempResult).append(Constants.NEW_LINE);
        }
        return result.toString();
    }

    /**
     * Converts Option to String for representing final result.
     * @param option
     * @return Items index as String
     */
    private static String optionToString(Option option){
        StringBuilder result = new StringBuilder();
        if(option.getItems().isEmpty()) return result.append("-").toString();
        result.append(option.getItems().iterator().next());
        option.getItems().stream().skip(1).forEach(item -> result.append(",").append(item));
        return result.toString();
    }

}
