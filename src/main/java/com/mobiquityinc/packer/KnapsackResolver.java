package com.mobiquityinc.packer;

import com.mobiquityinc.model.Knapsack;
import com.mobiquityinc.model.Option;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public interface KnapsackResolver {

    /**
     * Calculate the best selection of items in order to have maximum price.
     * @param knapsack
     * @return The best selections as Option
     */
    Option resolve(Knapsack knapsack);

}
