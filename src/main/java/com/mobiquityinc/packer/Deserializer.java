package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Knapsack;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public interface Deserializer {

    /**
     * Read data from a text file and convert them to a list of knapsacks.
     * @param filePath
     * @return list of knapsacks
     * @throws APIException
     */
    List<Knapsack> deserialize(String filePath) throws APIException;

}
