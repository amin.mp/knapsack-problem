package com.mobiquityinc.config;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class Constants {
    public static final String EMPTY = "";
    public static final String SPACE = "\\s";
    public static final String NO_ANSWER = "-\n";
    public static final String NEW_LINE = "\n";
    public static final String PARENTHESIS_REGEX = "\\((.*?)\\)";
    public static final int KNAPSACK_CAPACITY_INDEX = 0;
    public static final int KNAPSACK_ITEMS_INDEX = 1;
    public static final int ITEM_INDEX = 0;
    public static final int ITEM_WEIGHT_INDEX = 1;
    public static final int ITEM_PRICE_INDEX = 2;


}
