package com.mobiquityinc.model;

import java.math.BigDecimal;
import java.util.Set;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class Knapsack {
    private Set<Item> items;
    private BigDecimal capacity;

    public Knapsack(Set<Item> items, BigDecimal capacity) {
        this.items = items;
        this.capacity = capacity;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public BigDecimal getCapacity() {
        return capacity;
    }

    public void setCapacity(BigDecimal capacity) {
        this.capacity = capacity;
    }
}
