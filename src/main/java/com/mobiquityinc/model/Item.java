package com.mobiquityinc.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class Item implements Comparable<Item> {
    private Integer index;
    private BigDecimal weight;
    private BigDecimal price;

    public Item(Integer index, BigDecimal weight, BigDecimal price) {
        this.index = index;
        this.weight = weight;
        this.price = price;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return Objects.equals(index, item.index);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index);
    }

    @Override
    public int compareTo(Item o) {
        int compare = this.weight.compareTo(o.weight);
        if(compare == 0) return o.price.compareTo(this.price);
        return compare;
    }
}
