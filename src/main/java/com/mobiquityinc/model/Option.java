package com.mobiquityinc.model;

import java.math.BigDecimal;
import java.util.Set;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 10 Mar 2019
 */
public class Option implements Comparable<Option>{
    private Set<Integer> items;
    private BigDecimal capacity;
    private BigDecimal price;

    public Option(Set<Integer> items, BigDecimal capacity, BigDecimal price) {
        this.items = items;
        this.capacity = capacity;
        this.price = price;
    }

    public Set<Integer> getItems() {
        return items;
    }

    public void setItems(Set<Integer> items) {
        this.items = items;
    }

    public BigDecimal getCapacity() {
        return capacity;
    }

    public void setCapacity(BigDecimal capacity) {
        this.capacity = capacity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public int compareTo(Option o) {
        int compare = o.getCapacity().compareTo(this.getCapacity());
        if(compare == 0) return o.price.compareTo(this.price);
        return compare;
    }
}
